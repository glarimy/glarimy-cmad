package com.glarimy.cmad;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DirectoryControllerTest {

	@MockBean
	private EmployeeRepository repo;

	@Autowired
	private DirectoryController controller;

	@Test
	public void testCreateEmployee() {
		Employee employee = new Employee();
		employee.setId(123);
		employee.setName("ABC");
		employee.setPhoneNumber(123455L);

		when(repo.save(employee)).thenReturn(employee);

		ResponseEntity<Employee> response = controller.add(employee);

		assertTrue(response.getStatusCode() == HttpStatus.CREATED);
	}
}
