package com.glarimy.cmad;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@RestController
@CrossOrigin
@EnableSwagger2
public class DirectoryController {
	@Autowired
	private EmployeeRepository repo;

	@RequestMapping(path = "/employee", method = RequestMethod.POST)
	public ResponseEntity<Employee> add(@RequestBody Employee employee) {
		repo.save(employee);
		return new ResponseEntity<Employee>(HttpStatus.CREATED);
	}

	@RequestMapping(path = "/employee/{id}", method = RequestMethod.GET)
	public ResponseEntity<Employee> find(@PathVariable int id) {
		try {
			Employee employee = repo.findById(id).orElseThrow(() -> new Exception());
			return new ResponseEntity<Employee>(employee, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(path = "/employee", method = RequestMethod.GET)
	public ResponseEntity<List<Employee>> search(@RequestParam(value = "name") String name,
			@RequestParam(value = "size", defaultValue = "50") int size) {
		List<Employee> employees = repo.findByName(name);
		return new ResponseEntity<List<Employee>>(employees, HttpStatus.OK);
	}
}
