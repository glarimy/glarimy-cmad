package com.glarimy.cmad;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeServer {
	public static void main(String[] args) {
		SpringApplication.run(EmployeeServer.class);
	}
}
